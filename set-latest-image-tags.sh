#!/bin/bash
# if you get this error: 
# E: Unable to locate package jq
# Then run:
# sudo pico /etc/apt/sources.list
# Add this line at the bottom:
# deb http://us.archive.ubuntu.com/ubuntu vivid main universe
# Then run:
# sudo apt-get update
# sudo apt-get install jq

# get latest image tags
for repo in `curl -L -s -u 'readonly:Gfn@123' -X GET 'registry-web.devportal.dalet.cloud/v2/_catalog?n=100000' | jq -r .[][] | grep -E '^webnews/agents/|^webnews/apps/'`; do echo "registry-web.devportal.dalet.cloud:${repo}:"$(curl -L -s -u "readonly:Gfn@123" -X GET "registry-web.devportal.dalet.cloud/v2/${repo}/tags/list" | jq -r .tags[] | egrep '[[:digit:]]+\.'[[:digit:]]+\.'[[:digit:]]+(-4|$)' | sort -t "." -k1,1n -k2,2n -k3,3n | tail -n 1); done > tmp-images.txt

set_image_version () {
# get the image version from file tmp-images.txt
NEW_IMAGE_VERSION=`grep /$2: -F tmp-images.txt | awk -F':' '{print $3}'`
OLD_IMAGE_VERSION=`grep $1 -F .env | awk -F'=' '{print $2}'`
echo $1 = ${NEW_IMAGE_VERSION}
# set the image version to file .env
sed -i "s/^\($1=\).*/\1${NEW_IMAGE_VERSION}/" .env
}

set_image_version PLANNING_APP_IMAGE_VERSION       planning-app 
set_image_version PLANNING_GQL_IMAGE_VERSION       planning-gql
set_image_version WEBNEWS_GQL_IMAGE_VERSION        webnews-gql-server 
set_image_version RUNDOWN_GQL_IMAGE_VERSION        rundown-gql
set_image_version DB_SERVER_CPP_IMAGE_VERSION      dbserver-cpp
set_image_version ASSIGNEMT_NOTIFICATIONS_VERSION  assignment-notification-gql-server
set_image_version MEDIAURLSERVICE_IMAGE_VERSION    mediaurlservice
set_image_version FASTER_APP_IMAGE_VERSION         faster-app
set_image_version FASTER_TRIMMER_APP_IMAGE_VERSION faster-trimmer-app
set_image_version WEBEDIT_REST_IMAGE_VERSION       webedit-rest
set_image_version MOSGATEWAY_REST_IMAGE_VERSION    mosgateway-agent-rest
set_image_version USER_METADATA_IMAGE_VERSION      user-metadata-agent-gql-server
set_image_version JOB_MONITOR_IMAGE_VERSION        job-monitor-service
set_image_version SOLR_AGENT_IMAGE_VERSION         solr-index-server-agent