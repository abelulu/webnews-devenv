#!/bin/bash

# $1 - which entry from .myEnv to read
# $2 - in which file to replace
# $3 - search pattern
# $4 - output pattern
set_env_data () {
# get the image version from file tmp-images.txt
NEW_VALUE=`grep $1 -F .myEnv | cut -d "=" -f2`
SEARCH_PATTERN=$3
OUT_PATTERN=$4
NEW_LINE=`echo ${OUT_PATTERN//XXX/$NEW_VALUE}`

echo [$2] $1 = ${NEW_VALUE}
# echo SEARCH_PATTERN $SEARCH_PATTERN
# echo OUT_PATTERN ${OUT_PATTERN}
echo ${NEW_LINE}
echo -------------------------
# set the new value to the file image version to file $2
sed -i "s/.*$SEARCH_PATTERN.*/${NEW_LINE}/g" $2
}

# galaxyconfig.properties
set_env_data GALAXY_SITE_NAME       config-galaxy/galaxyconfig.properties GALAXY_SITE_NAME GALAXY_SITE_NAME=XXX
set_env_data GALAXY_DB_ADMIN_USER   config-galaxy/galaxyconfig.properties GALAXY_DB_ADMIN_USER GALAXY_DB_ADMIN_USER=XXX
set_env_data GALAXY_DB_ADMIN_PWD    config-galaxy/galaxyconfig.properties GALAXY_DB_ADMIN_PWD GALAXY_DB_ADMIN_PWD=XXX
set_env_data GALAXY_SQL_SERVER_NAME config-galaxy/galaxyconfig.properties GALAXY_SQL_SERVER GALAXY_SQL_SERVER=XXX
set_env_data GALAXY_SQL_PORT        config-galaxy/galaxyconfig.properties GALAXY_SQL_PORT GALAXY_SQL_PORT=XXX
set_env_data GALAXY_SQL_DB_NAME     config-galaxy/galaxyconfig.properties GALAXY_SQL_DB_NAME GALAXY_SQL_DB_NAME=XXX
set_env_data GALAXY_SQL_DB_TYPE     config-galaxy/galaxyconfig.properties GALAXY_SQL_DB_TYPE GALAXY_SQL_DB_TYPE=XXX

# .env
set_env_data GALAXY_SQL_SERVER_NAME .env GALAXY_SQL_SERVER_NAME GALAXY_SQL_SERVER_NAME=XXX
set_env_data GALAXY_SQL_SERVER_IP   .env GALAXY_SQL_SERVER_IP GALAXY_SQL_SERVER_IP=XXX
set_env_data GALAXY_HOST_NAME       .env GALAXY_HOST_NAME GALAXY_HOST_NAME=XXX
set_env_data GALAXY_HOST_IP         .env GALAXY_HOST_IP GALAXY_HOST_IP=XXX

# appsettings.json
set_env_data DOCKERS_HOST_NAME appsettings.json graphqlHostName "\"graphqlHostName\":\"XXX\","
set_env_data DOCKERS_HOST_NAME appsettings.json solrUrl "\"solrUrl\": \"XXX\","
set_env_data SOLR_INDEX_NAME   appsettings.json titlesIndex "\"titlesIndex\": \"XXX\""

# webnews-devenv.html
set_env_data DOCKERS_HOST_NAME webnews-devenv.html http://XXX:
