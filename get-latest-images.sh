#!/bin/bash
# if you get this error: 
# E: Unable to locate package jq
# Then run:
# sudo pico /etc/apt/sources.list
# Add this line at the bottom:
# deb http://us.archive.ubuntu.com/ubuntu vivid main universe
# Then run:
# sudo apt-get update
# sudo apt-get install jq
for repo in `curl -L -s -u 'readonly:Gfn@123' -X GET 'registry-web.devportal.dalet.cloud/v2/_catalog?n=100000' | jq -r .[][] | grep -E '^webnews/agents/|^webnews/apps/'`; do echo "registry-web.devportal.dalet.cloud:${repo}:"$(curl -L -s -u "readonly:Gfn@123" -X GET "registry-web.devportal.dalet.cloud/v2/${repo}/tags/list" | jq -r .tags[] | egrep '[[:digit:]]+\.'[[:digit:]]+\.'[[:digit:]]+(-4|$)' | sort -t "." -k1,1n -k2,2n -k3,3n | tail -n 1); done