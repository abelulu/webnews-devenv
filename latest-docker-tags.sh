#/bin/bash
# If jq doesnt work run the following:
# sudo vim /etc/apt/sources.list
# Add the following line to the end of that file (note deb is not a command, more info):
# deb http://us.archive.ubuntu.com/ubuntu vivid main universe
# sudo apt-get update
# sudo apt-get install jq
# see also here:
# https://stackoverflow.com/questions/33184780/install-jq-json-processor-on-ubuntu-10-04
for repo in `curl -s -u 'readonly:Gfn@123' -X GET 'registry-web.devportal.dalet.cloud:5000/v2/_catalog?n=100000' | jq -r .[][] | grep -E '^webnews/agents/|^webnews/apps/'`; do echo "registry-web.devportal.dalet.cloud:${repo}:"$(curl -s -u "readonly:Gfn@123" -X GET "registry-web.devportal.dalet.cloud:5000/v2/${repo}/tags/list" | jq -r .tags[] | egrep '[[:digit:]]+\.'[[:digit:]]+\.'[[:digit:]]+(-4|$)' | sort -t "." -k1,1n -k2,2n -k3,3n | tail -n 1); done
