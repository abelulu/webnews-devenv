# Setting up WebNews Development Environment

## Setting up docker-compose environment
Docker-compose can be used to run backend agents in development environment.

**Prerequisits**: Docker installed (can be either on a linux machine or on [windows with WSL2](https://docs.docker.com/docker-for-windows/install/)).

You can use the content of [webnews-devenv](https://bitbucket.org/ctabochdalet/webnews-devenv/src/master/) git repository to get all the files you need to run WebNews agents in development environment.
Start by cloning this repository:

```
$> git clone git@bitbucket.org:ctabochdalet/webnews-devenv.git
$> cd webnews-devenv
```

## Docker Registry

### Docker Registry URLs

- [https://registry-web-ui.devportal.dalet.cloud/](https://registry-web-ui.devportal.dalet.cloud/)

auth: svc/Gfn@123

### Login to docker repository

```
$> docker login registry-web.devportal.dalet.cloud
Username: svc
Password Gfn@123
```

### Main Docker Images

Check those registries to know what is the latest version to install:

* [Planning agent GQL](https://registry-web-ui.devportal.dalet.cloud/#!/taglist/webnews/agents/planning-gql)
* [Rundown-gql](https://registry-web-ui.devportal.dalet.cloud/#!/taglist/webnews/agents/rundown-gql)
* [Planning-app](https://registry-web-ui.devportal.dalet.cloud/#!/taglist/webnews/apps/planning-app)
* [Planning-app-dev](https://registry-web-ui.devportal.dalet.cloud/#!/taglist/webnews/apps/planning-app-dev)
* [Webnews-gql](https://registry-web-ui.devportal.dalet.cloud/#!/taglist/webnews/agents/webnews-gql-server)

You can also run:
```
$> ./set-latest-image-tags.sh
```
It will update .env file with the latest image versions.

## General Configuration Files

Following section describes configuration files used in this environment.
Some of them should be edited to fit your environment.

### appsettings.json
Contains configuration settings for backend graphql servers, it is used by planning-app.
Set the hostname to the host where the webnews containers run (this host should be accessible from the web browser).
If you changed the ports in the .env file then update them as well.
Update solr url according to Galaxy solr server.


### Folder: agents-certs

Contains following certificate files:

- ca.crt
- client.crt
- client.key

(No editing required here).

### Folder: config-galaxy

- **galaxy-addresses.conf** - list of servers running on Galaxy site.
```
ThumbnailsServer_container,Pyramid_Lena-ThumbnailsServer_1@XP15-HF40352,QA-LENA.dalet.local,8505,tls,be065b1d-e4ed-4cba-a3c2-2273c9322104
FrameAccurateStreamingServer,Pyramid_Lena-FrameAccurateStreamingServer_1@XP15-HF40352,QA-LENA.dalet.local,8507,tls,ee790e18-de01-4e53-9aef-78ea5c612c90
```
The content of this file should be edited in case there are servers running on windows Galaxy site that should be accessed from webnews agents.

- **galaxyconfig.properties** - site and database connection info.
```
GALAXY_SITE_NAME=Pyramid_Lena

GALAXY_DB_ADMIN_USER=dalet_admin
GALAXY_DB_ADMIN_PWD=dalet_admin
GALAXY_SQL_SERVER=sql2k14.dalet.local
GALAXY_SQL_PORT=1433
GALAXY_SQL_DB_NAME=Pyramid_Lena
GALAXY_CLUSTER_USER=adsv01
GALAXY_CLUSTER_IDENT=Gfn@123
```
The content of this file should be edited according to your database parameters.


- **webnews-addresses.conf** - list of servers running on Webnews site.
```
DbServer,COHAVIT,dbserver-1,8501,plain
NatsNotifierServer,COHAVIT,nats-1,4222,plain
```
The format is:
```
<ServerType>,<SiteName>,<Host>,<Port>,<plain/tls>
```
The content of this file should be edited for connection between servers running in webnews cluster.
The host name is the name of the docker container, and the port is the **internal** agent port.

### Folder: config-galaxy-legacy

Contains files that should be **copied** to legacy Galaxy nameserver machine unning on windows in C:\ProgramData\Dalet\Data.
This allows Galaxy agents and apps to connect to agents on webnews cluster.
The host name in this case is the linux machine where the webnews docker containers are running, and the ports are the external ports.

- **addresses.ini** - addresses of webnews servers. In format: serverclass,sitename|agentname,host|fqdn,port,[tls|plain]
For example:
```
NatsNotifierServer, COHAVIT, dev-cohavit.dalet.local, 4222, plain
DbServer,COHAVIT, dev-cohavit.dalet.local, 8520, plain
```

Note: when docker containers running on local machine, sometimes the external name of the machine won't work.
You should check names of other netowkr adapters and set them instead. For example in my case this worked:
```
NatsNotifierServer,COHAVIT,kubernetes.docker.internal,4222,plain
DbServer,COHAVIT,kubernetes.docker.internal,8520,plain
```


- **config.ini** - address of name server and additional flags
```
[NAME_SERVICE]
USE_SERVERS=DEV-COHAVIT.dalet.local
[DTKFlags]
NATS_USE_SSL=0
CERTIFICATE_AUTH_MODE=1
```
For more info, see also: [DCL Route Galaxy to WebNews]https://www.devportal.dalet.cloud/en/WebNews/Ops/GalaxyWebNewsRouting

### Specific agents configuration files.

For each agent there is a folder with its own config.

#### dbserver-config

- agentconfig.properties:
  
GALAXY_SERVICE_NAME - contains server external name in form sitename-agentname_num@HOSTNAME (set according to your site):

DB_DOMAIN_NAME - Change it if this DbServer runs in a special DbDomain (can remain empty in development environment).

```
# Agent specific options
# Service Name, usually <sitename-agentname_num@HOSTNAME>
GALAXY_SERVICE_NAME=Pyramid_Lena-DbServer_1@EPHEMERALHOST
DB_DOMAIN_NAME=realtime
```

#### planningagent-config

- agentconfig.properties - contains listening port (no need to change):

```
# Web App Port, default is 8460
WEBAPP_PORT=8460
```

#### rundownagent-config

- agentconfig.properties - contains listening port (no need to change):

```
# Agent specific options
# Service Name, usually <sitename-agentname_num@HOSTNAME>
WEBAPP_PORT=9000
```

#### jobmonitor-config

- agentconfig.properties - contains listening port (no need to change):

```
# Agent specific options
# Service Name, usually <sitename-agentname_num@HOSTNAME>
WEBAPP_PORT=9090
```

#### solrindexserver-config

- agentconfig.properties - contains configuration data for solr agent.
The entry **GALAXY_SERVICE_NAME** should be set according to the solr external name in Galaxy.
This will determine which deployment will be used for this agent and using the deployment data from the database, the agent knows also which index to use.

```
# Service Name, usually <sitename-agentname_num@HOSTNAME> - according to this server name deployment in Galaxy, this solr will pick the index to use.
GALAXY_SERVICE_NAME=COHAVIT-SolrIndexServer_1@DEV-COHAVIT
BATCH_SIZE=25
ROUND_SIZE=2000
SLEEP_TIME_BETWEEN_ROUNDS=0
JETTY_MANAGEMENT_PORT=8983
# If categories should be indexed by this SolrIndexServer
SHOULD_INDEX_CATEGORIES=true
# Max delay between index updates for categories (in case no notification is received before). In minutes
MAX_DELAY_BETWEEN_CATEGORIES_INDEX_UPDATES=60
```

## Docker Compose Files

### docker-compose.yml

In some cases, only few entries should be changed in this file:

- **extra_hosts**
  This entry is used in case the Galaxy nameserver host name is not recognized inside the docker container.
  You can comment it out if the GQL agents manage to connect to Galaxy by hostname.
  If they fail to connect, then set up GALAXY_HOST_NAME and GALAXY_HOST_IP in the .env file (see below).
  You might need to try more than one IP in case Galaxy machine has several adapters.
- **Graphql agents command line**
  You might need to change the command line of graphql agents connecting to Galaxy, depends on the connection mode.

  	- **For SQL:**
  	command: RundownGQL -site ${GALAXY_SITE} -connectionmode SQL
  	- **For Active Directory:**
  	command: RundownGQL -site ${GALAXY_SITE} -connectionmode CU

### .env

- Configure the docker image tags - needed only when updating to new versions.
- Set the SQL server name and IP: (The host name and IP are required for extra_hosts in docker-compose, which can be removed if the host is accessible by name from the container).
```
GALAXY_SITE=COHAVIT
GALAXY_SQL_SERVER_NAME=dev-cohavit.dalet.local
GALAXY_SQL_SERVER_IP=192.168.56.1
```

## Running

### Start/Stop

- Start:
```
$> docker-compose up
```
- Stop:
Hit Ctrl+C in the same shell where it was executed, or:
```
$> docker-compose stop
```

### Home Page

Open the page **webnews-devenv.html** in web browser use it to access web ui of agents and apps.


### Cleaning up old images

```
$> docker image prune
```

### Logs and Monitoring

From cli:

```
$> sudo docker-compose logs --tail 200 | grep planning-gql
```  

In this system, there are 2 docker logging and monitoring tools installed:

- [dozzle](https://dozzle.dev/)
- [portainer](https://www.portainer.io/)

The web ui of those tools is accessible from **webnews-devenv.html**. You can use the web UI to view logs and manage the containers.

If you are using docker-desktop, the UI allows displaying logs, executing shell and starting/stopping containers.

### Authentication

The current planning-gql agent does not manage authentication.
In order to allow connecting to this platform, you need to use [ModHeader](https://chrome.google.com/webstore/search/modheader) chrome extension.
The user-metadata-agent requires additional 2 headers to be set properly.

In ModHeader set following request headers for authentication:

- **X-Pomerium-Claim-Email** <user_login>
- **x-pomerium-jwt-assertion** <ayntoken>
- **x-pomerium-claim-id** <user id of dalet_admin from keycloak>

In keycloak: users administration -> Attributes:
 Add key "userID" and set as value the Galaxy user id (run SQL in Galaxy DB "select user_id from users where login_name = 'dalet_admin').


### Solr Port
Solr uses the same port that was configured in Galaxy in the server deployment. 
You can find the solr port the agent uses from the logs:
```
$> docker-compose logs | grep solr | grep getPortForJetty
```  
Check that you can connect to solr via web browser:
http://dev-cohavit:8485/solr/#/

You can find the index names in the core selector combo box.

Set address, port and index names in appsettings.json:
```
    "solrServers": [
        {
            "serverName": "media-browser-solr-server",
            "solrUrl": "http://dev-cohavit:8485",
            "solrIndexes": {
                "categoriesIndex": "CategoriesIndex",
                "titlesIndex": "Index2_1_English"
            }
        }
```

In your web browser, install "CORS Unblock" extension to prevent CORS errors when accessing solr.

### Browsing to the app

- **Production image:** http://dev-cohavit:4350/framework
- **Dev image:** http://dev-cohavit:4360/framework

The ports are configured in **PLANNING_APP_PORT** and **PLANNING_APP_DEV_PORT**.

### Using PGAdmin

- In servers right click "create server".
- In connection tab:
    - When using pgadmin installed locally (on developer machine):
        - set host to **<dockers-hostname>**
        - Port - according to the .env file (WEBNEWS_POSTGRES_PORT = 4352).
        - Username: postgres
        - Password: Gfn!12345
    - When using pgadmin container
        - set host to **wn-postgresql**
        - port 5432 
        - user postgres
        - password Gfn!12345

### Keycloak Admin

- Browse to: http://dev-cohavit:5530
- Click on "Administration Console"
- Login with credentials: admin / admin.
- On top left menu click on "Create realm" and create a new realm with name webnews.
- On Manage menu you can create users and groups and join users to groups.
- For planning activity, create folowing groups and associate users to them:
    - PUBLIC
    - Administrators
    - PLANNING
    - Digital Journalists
    - TV Journalists


### DbServer Periodical Restart
On virtual box we often see that the machine hangs. We suspect dbserver.
Therefore we set an automatic restart of dbserver every 20 min using crontab.

Run:

```
crontab -e
```

Pick an editor.
Add the following line at the end of the file:

```
*/20 * * * * /vagrant/webnews-devenv/restart-db-server.sh
```


## Troubleshooting

- Make sure that all config files have newlines in unix format.
galaxyconfig.properties in bad format might cause failure in liquibase.
It can be fixed using notepad++ menu Edit-> EOL Conversion -> unix.

- Make sure that host mapping is set correctly to any external hosts used. 
    - In docker-compose.yml, you can uncomment  **host-access-checker** and use it to check if the host is accessible.
    - If the host is not accessible, try to change it to IP address.
    - If IP address does not work, run ipconfig and try one of the other IP address available, maybe the main address is not accessible from docker but one of the other IPs is accessible.


- Make sure that modeheader configuration is correctly set with following entries:
    - x-pomerium-jwt-assertion
    - x-pomerium-claim-id
    - X-Pomerium-Claim-Email

- WSL takes too much memory or cpu. Set the desires parameters on file "C:\Users\<your user name>\.wslconfig"

```
[wsl2]
memory=4GB # Limits VM memory in WSL 2 to 4 GB
processors=2 # Makes the WSL 2 VM use two virtual processors
```

And then, from Powershell with admin rights, restart WSL2 by typing:

```
Restart-Service LxssManager
```

- Running docker-compose up on WSL shows such errors:

```
  Traceback (most recent call last):
  File "docker\api\client.py", line 214, in _retrieve_server_version
  File "docker\api\daemon.py", line 181, in version
  File "docker\utils\decorators.py", line 46, in inner
  File "docker\api\client.py", line 237, in _get
  File "requests\sessions.py", line 543, in get
  ...
  File "docker\transport\npipesocket.py", line 52, in connect
pywintypes.error: (2, 'CreateFile', 'The system cannot find the file specified.')
```

Make sure docker desktop is running and execute docker login.

