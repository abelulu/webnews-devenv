#!/bin/bash
pushd /vagrant/webnews-devenv
echo restarting dbserver-1 >> /vagrant/webnews-devenv/dbserver-restarts.log
date >> /vagrant/webnews-devenv/dbserver-restarts.log
sudo docker-compose restart dbserver-1
popd